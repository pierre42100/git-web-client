# GitWebClient
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

## Build for debugging
1. Adapt the configuration of the git server. Make sure the line `access_control_allow_origin: "*"` is present in the config
2. Modify the file `src/environment/environment.ts` to point to your running instance of Git server
3. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Integrate to a Git server in release mode
1. Install & configure your Git Server.
2. On your machine, install NodeJS, NPM
3. Run `npm install`
4. Run `ng build --configuration production`.
5. Copy the `dist/git-web-client` directory wherever you want in your server
6. Adapt the configuration of the Git server: Make sure the `front_end_files` property of your configuration points to the directory that contains the built files
7. Restart the server

## Build in Docker
1. Run `docker run --rm -ti -v $(pwd):/app node:18 /bin/bash`
2. Run in container:
```bash
cd /app
npm install
npm run build
```
