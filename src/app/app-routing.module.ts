import { NgModule } from '@angular/core';
import { Routes, RouterModule, UrlSegment } from '@angular/router';
import { ReposListComponent } from './repos-list/repos-list.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RepoNavigationComponent } from './repo/repo-navigation/repo-navigation.component';
import { joinURLSegments } from './UrlUtils';
import { ReposList } from 'src/api/ReposList';

const routes: Routes = [

  // Main route
  {
    path: "",
    component: ReposListComponent,
  },

  // Repo routes
  {
    component: RepoNavigationComponent,
    matcher: (r) => {
      const uri = joinURLSegments(r);

      for(const repo of ReposList.GetList())
      {
        if (uri != repo && !uri.startsWith(repo + "/"))
          continue;
        
        const consumed = [];
        for (let i = 0; i < repo.split("/").length; i++)
          consumed.push(r[i]);
        
        return {
          consumed: consumed,
          posParams: {
            "repo": new UrlSegment(repo, {})
          }
        };
      }

      return null;
    },
    loadChildren: () => import("./repo/repo.module").then(mod => mod.RepoModule)
  },

  // Repos list routes
  {
    component: ReposListComponent,
    matcher: (r) => {
      const uri = joinURLSegments(r);

      for(const repo of ReposList.GetList())
      {
        if (!repo.startsWith(uri))
          continue;
        
        return {
          consumed: r,
          posParams: {
            "dir": new UrlSegment(uri, {})
          }
        }
      }

      return null;
    },
  },

  // Dispatcher - Not found
  {
    path: "**",
    component: NotFoundComponent,
    //canActivate: [NotFoundGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
