import { Component, OnInit, Input } from '@angular/core';
import { TreeFile } from 'src/api/TreeAPI';
import { determineRawURL, determineHistoryURL } from 'src/app/UrlUtils';
import { FilesAPI } from 'src/api/FilesAPI';
import { HighlightService } from '../highlight.service';
import { isImagePath, fileLanguageType } from 'src/utils/FileUtils';

@Component({
  selector: 'app-tree-file',
  templateUrl: './tree-file.component.html',
  styleUrls: ['./tree-file.component.less']
})
export class TreeFileComponent implements OnInit {
  
  @Input("repo") repo: string;
  @Input("rev") rev: string;
  @Input("file") file: TreeFile;
  
  private isBinary = true;
  fileContent: string = null;
  private highlighted = false;
  
  get isImage() : boolean
  {
    return this.isBinary && isImagePath(this.file.name)
  }
  
  get isRawBinary() : boolean
  {
    return this.isBinary && !this.isImage
  }
  
  get isMarkdown() : boolean
  {
    return !this.isBinary && this.file.path.toLowerCase().endsWith(".md")
  }
  
  get language() : string
  {
    if(this.isBinary)
      return null;
    
    return fileLanguageType(this.file.name);
  }
  
  get isSourceCodeFile() : boolean
  {
    return this.language != null;
  }
  
  get isPlainText() : boolean
  {
    return !this.isBinary && !this.isMarkdown && !this.isSourceCodeFile;
  }
  
  get rawFilePath() : string
  {
    return determineRawURL(this.repo, this.rev, this.file.path, false)
  }

  /**
   * Access current file history
   */
  get historyURL() : string
  {
    return determineHistoryURL(this.repo, this.rev, this.file.path);
  }
  
  constructor(private highlightService: HighlightService) { }
  
  ngOnInit(): void {
    
  }
  
  ngOnChanges() {
    this.init();
  }
  
  ngAfterViewChecked() {
    if (this.fileContent && !this.isBinary && !this.highlighted) {
      this.highlightService.highlightAll();
      this.highlighted = true;
    }
  }
  
  /**
  * Initialize component
  */
  private async init()
  {
    this.fileContent = null;
    this.highlighted = false;
    this.isBinary = await FilesAPI.IsBinary(this.repo, this.rev, this.file.path);
    
    if (!this.isRawBinary)
    {
      this.fileContent = await (await fetch(this.rawFilePath)).text();
    }
  }
  
  /**
  * Open the file in raw format
  * 
  * @param download Specify whether the file should be downloaded
  */
  raw(download: boolean) : void
  {
    location.href = determineRawURL(this.repo, this.rev, this.file.path, download);
  }
}
