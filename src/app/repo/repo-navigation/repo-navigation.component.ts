import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay, startWith } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { RevStats, StatisticsAPI } from 'src/api/StatisticsAPI';
import { TagsAPI } from 'src/api/TagsAPI';
import { BranchesAPI } from 'src/api/BranchesAPI';
import { determineCurrentRevision, determineCurrentRepoPath } from 'src/app/UrlUtils';
import { FormControl } from '@angular/forms';
import { ReposList } from 'src/api/ReposList';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';

interface MenuIcon
{
  title: string,
  icon: string,
  badge ?: string,
  link: string
}

@Component({
  selector: 'app-repo-navigation',
  templateUrl: './repo-navigation.component.html',
  styleUrls: ['./repo-navigation.component.less']
})
export class RepoNavigationComponent {

  repoPath : string;
  currRev : string = "master";
  stats? : RevStats;
  tags? : string[];
  branches? : string[];

  // Repo quick access
  searchValue: string;
  searchControl = new FormControl();
  options: string[] = ReposList.GetList();
  filteredOptions: Observable<string[]>;

  get repoName() : string
  {
    if (!this.repoPath) return "";

    return this.repoPath.split("/").slice(-1).pop()
  }

  get repoParentDir() : string
  {
    if(!this.repoPath) return "";

    const parts = this.repoPath.split("/");
    parts.pop();
    return parts.join("/");
  }

  get menuEntries() : Array<MenuIcon>
  {
    if (!this.repoPath) return [];
    
    return [
      {
        title: "Tree",
        icon: "code",
        link: "/" + this.repoPath + "/tree/" + this.currRev
      },

      {
        title: "Commits",
        icon: "history",
        badge: this.stats?.number_commits.toString(),
        link: "/" + this.repoPath + "/commits/" + this.currRev
      },

      {
        title: "Branches",
        icon: "toc",
        badge: this.branches?.length.toString(),
        link: "/" + this.repoPath + "/branches",
      },

      {
        title: "Tags",
        icon: "label",
        badge: this.tags?.length.toString(),
        link: "/" + this.repoPath + "/tags",
      },
    ];
  }
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  
  constructor(private breakpointObserver: BreakpointObserver, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() : void {
    this.route.params.subscribe(p => {
      this.repoPath = determineCurrentRepoPath(this.router);
    });

    this.route.url.subscribe( p => {
      this.load();
    });

    this.filteredOptions = this.searchControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  quickOpenRepo(e: MatAutocompleteSelectedEvent) {
    this.router.navigateByUrl("/" + e.option.value)
    this.searchControl.setValue("")
  }

  private async load() {
    this.currRev = await determineCurrentRevision(this.route, this.router);
    this.stats = await StatisticsAPI.GetStats(this.repoPath, this.currRev);
    this.tags = await TagsAPI.GetList(this.repoPath);
    this.branches = await BranchesAPI.GetList(this.repoPath);
  }
}
