import { Pipe, PipeTransform } from '@angular/core';
import { Commit } from 'src/api/CommitsAPI';

const MESSAGES_LENGTH = 35;

@Pipe({
  name: 'commitMessageBegin',
  pure: true,
})
export class CommitMessageBeginPipe implements PipeTransform {

  transform(commit: Commit): string {
    const msg = commit.firstLineComment;
    return msg.length <= MESSAGES_LENGTH ? msg : msg.substr(0, MESSAGES_LENGTH - 3) + "...";
  }

}
