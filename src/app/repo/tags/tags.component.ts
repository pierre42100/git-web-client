import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TagsAPI } from 'src/api/TagsAPI';
import { determineCurrentRepoPath } from 'src/app/UrlUtils';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.less']
})
export class TagsComponent implements OnInit {

  repoPath : string
  tags: string[] = []

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe( () => {
      this.repoPath = determineCurrentRepoPath(this.router);
    });

    TagsAPI.GetList(this.repoPath).then(t => this.tags = t)
  }
}
