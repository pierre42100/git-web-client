import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filesize',
  pure: true
})
export class FilesizePipe implements PipeTransform {

  transform(value: number): string {
    if (value < 10000)
      return (value / 1000) + " KB";

    return (value / 1000000) + " MB";
  }

}
