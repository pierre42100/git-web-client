import { Component, OnInit } from '@angular/core';
import { determineCurrentRepoPath, determineCurrentRevision } from 'src/app/UrlUtils';
import { ActivatedRoute, Router } from '@angular/router';
import { StatisticsAPI } from 'src/api/StatisticsAPI';
import { PageEvent } from '@angular/material/paginator';
import { Commit, CommitsAPI } from 'src/api/CommitsAPI';

const PAGES_SIZE = 50;

@Component({
  selector: 'app-commits-list',
  templateUrl: './commits-list.component.html',
  styleUrls: ['./commits-list.component.less']
})
export class CommitsListComponent implements OnInit {

  rev: string;
  repoPath: string;
  filePath: string;
  pageNumber: number;
  totalCommits: number;

  commits: Commit[];

  get numberPages() : number
  {
    return Math.ceil(this.totalCommits / this.pageSize)
  }

  get isForFile() : boolean
  {
    return this.filePath != null && this.filePath.length > 0;
  }

  get pageSize() : number
  {
    return PAGES_SIZE
  }

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.pageNumber = Number(params["page"]);

      if (!this.pageNumber)
        this.pageNumber = 0;

      this.init();
    });
  }

  private async init() : Promise<void>
  {
    this.commits = null;

    this.repoPath = determineCurrentRepoPath(this.router);
    this.rev = await determineCurrentRevision(this.route, this.router)

    // Attempt to extract file path from URL
    this.filePath = this.router.url.substr(("/" + this.repoPath + "/commits/" + this.rev).length).split("?")[0].split("#")[0];
    if (this.filePath.startsWith("/"))
      this.filePath = this.filePath.substr(1)
    
    if(!this.isForFile)
    {
      this.totalCommits = (await StatisticsAPI.GetStats(this.repoPath, this.rev)).number_commits;
      this.commits = await CommitsAPI.GetList(this.repoPath, this.rev, this.pageNumber * this.pageSize);
    }

    else
    {
      this.commits = await CommitsAPI.GetPathHistory(this.repoPath, this.rev, this.filePath);
      this.totalCommits = this.commits.length;
      
      this.commits = this.commits.slice(this.pageNumber*this.pageSize, (1 + this.pageNumber)*this.pageSize)
    }
  }

  /**
   * Change current page
   */
  changePage(e: PageEvent)
  {
    this.router.navigateByUrl("/" + this.repoPath + "/commits/" + this.rev + "/" + this.filePath + "?page=" + e.pageIndex)
  }
}
