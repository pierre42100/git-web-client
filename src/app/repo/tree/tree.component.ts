import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { determineCurrentRepoPath, determineCurrentRevision, determineCurrentTreePath, determineCloneURL, determineDownloadURL } from 'src/app/UrlUtils';
import { TreeFile, TreeAPI, FileType, TreeFileWithCommit } from 'src/api/TreeAPI';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Clipboard_CopyTo } from 'src/app/ClipboardUtils';
import { Commit } from 'src/api/CommitsAPI';
import { ObjectsAPI } from 'src/api/ObjectsAPI';

interface TreePathPart
{
  name: string,
  fullPath: string,
  isDir: boolean
}

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.less']
})
export class TreeComponent implements OnInit {

  repoPath: string;
  currRev: string;
  treePath: string;
  files: TreeFileWithCommit[];
  ready: boolean = false;
  destroyed = false;

  get repoName() : string
  {
    return this.repoPath.split("/").slice(-1)[0]
  }

  get treePathParts() : TreePathPart[]
  {
    let basePath = "/" + this.repoPath + "/tree/" + this.currRev;
    const paths = this.treePath.split("/");
    
    // In case of directory, remove the last element of the array
    if (paths[paths.length - 1] == "")
      paths.pop();
    
    const list =  paths.map(el => {
      if (!basePath.endsWith("/"))
        basePath += "/";

      basePath += el;

      return {
        name: decodeURIComponent(el == "" ? this.repoName : el),
        fullPath: basePath,
        isDir: true
      };
    });

    if (!this.treePath.endsWith("/"))
      list[list.length - 1].isDir = false;

    return list;
  }

  get isTreeView()
  {
    return this.treePath.endsWith("/")
  }

  get sortedFiles() : TreeFileWithCommit[]
  {
    return this.files.sort((a, b) => {
        if(a.file.type != b.file.type)
          return a.file.type == FileType.File ? 1 : -1;

        return a.file.name.localeCompare(b.file.name);
    })
  }

  /**
   * Get the current file to show
   */
  get fileView() : TreeFile
  {
    if (this.files.length == 1 && this.files[0].file.isFile)
      return this.files[0].file;
    
    const readme = this.files.find(f => f.file.name.toLowerCase().startsWith("readme"));
    return !readme ? null : readme.file;
  }

  // Fetch latest commit
  private lastCommitCache: Commit;
  get lastCommit(): Commit
  {

    if (this.lastCommitCache == null)
    {
      for (const file of this.files)
      {
        const commit = file.commit;

        if (!commit)
          continue;

        if(!this.lastCommitCache || commit.date.getTime() > this.lastCommitCache.date.getTime())
          this.lastCommitCache = commit;
      }
    }
    
    return this.lastCommitCache
  }

  constructor(private route: ActivatedRoute, private router: Router, private snackBar: MatSnackBar) {}

  ngOnInit() : void {
    this.route.params.subscribe(p => {
      this.load();
    });

    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd && !this.destroyed)
      {
        this.load();
      }
    });
  }

  ngOnDestroy()
  {
    this.destroyed = true;
  }

  private async load() {
    this.ready = false;
    this.lastCommitCache = null;

    this.repoPath = determineCurrentRepoPath(this.router);

    this.currRev = await determineCurrentRevision(this.route, this.router);
    this.treePath = determineCurrentTreePath(this.router);

    this.files = await TreeAPI.GetFilesWithCommits(this.repoPath, this.currRev, this.treePath);

    if (this.files.length == 1 && this.files[0].file.type == FileType.Directory)
    {
      this.treePath = this.treePath + "/";
      this.files = await TreeAPI.GetFilesWithCommits(this.repoPath, this.currRev, this.treePath);
    }

    this.ready = true;
  }

  /**
   * Clone the repository
   */
  public onClone()
  {
    Clipboard_CopyTo(determineCloneURL(this.repoPath))
    this.snackBar.open("URL copied to clipboard!", null, {
      duration: 2000
    })
  }

  /**
   * Download the repository
   */
  public onDownload(type)
  {
    location.href = determineDownloadURL(this.repoPath, this.currRev, type);
  }

  /**
   * Open the commit of a file
   */
  public openCommitForFile(e: any, file: TreeFileWithCommit)
  {
    e.stopImmediatePropagation();

    this.router.navigateByUrl("/" + this.repoPath + "/commit/" + file.commit.hash)
  }
}
