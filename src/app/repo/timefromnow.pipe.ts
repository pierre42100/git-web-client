import { Pipe, PipeTransform } from '@angular/core';
import { DateUtils } from 'src/DateUtils';

@Pipe({
  name: 'timefromnow',
  pure: true,
})
export class TimefromnowPipe implements PipeTransform {

  transform(value: Date): string {
    return DateUtils.timeDiffToStr(value.getTime() / 1000);
  }

}
