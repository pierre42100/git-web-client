import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommitFullInfoComponent } from './commit-full-info.component';

describe('CommitFullInfoComponent', () => {
  let component: CommitFullInfoComponent;
  let fixture: ComponentFixture<CommitFullInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommitFullInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommitFullInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
