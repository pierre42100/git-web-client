import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FullCommit, CommitsAPI } from 'src/api/CommitsAPI';
import { determineCurrentRepoPath } from 'src/app/UrlUtils';
import { HighlightService } from '../highlight.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-commit-full-info',
  templateUrl: './commit-full-info.component.html',
  styleUrls: ['./commit-full-info.component.less']
})
export class CommitFullInfoComponent implements OnInit {

  repoPath: string;
  hash: string;
  commit: FullCommit;
  highlighted = false;

  get ready() : boolean
  {
    return this.commit != null && this.hash != null && this.repoPath != null;
  }

  constructor(private router: Router, private route: ActivatedRoute, private highlightService: HighlightService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.route.params.subscribe(p => {
      this.hash = p.hash;
      this.init();
    });
  }

  ngAfterViewChecked() {
    if (this.ready && !this.highlighted) {
      this.highlightService.highlightAll();
      this.highlighted = true;
    }
  }

  private async init() : Promise<void>
  {
    this.commit = null;
    this.highlighted = false;
    this.repoPath = determineCurrentRepoPath(this.router);
    this.commit = await CommitsAPI.GetFullCommit(this.repoPath, this.hash);
  }

  async parentCommit(errDialog)
  {
    try {
      const commit = await CommitsAPI.GetParent(this.repoPath, this.commit.info.hash);
      this.router.navigateByUrl("/" + this.repoPath + "/commit/" + commit.hash)
    }
    catch(e)
    {
      console.error(e)

      let dialogRef = this.dialog.open(errDialog, {
        width: '250px',
        // data: { name: this.name, animal: this.animal }
      });

      dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          // this.animal = result;
      });
    }
  }

  browseFiles()
  {
    this.router.navigateByUrl("/" + this.repoPath + "/tree/" + this.hash)
  }

  showFullFile(path : string)
  {
    this.router.navigateByUrl("/" + this.repoPath + "/tree/" + this.hash + "/" + path)
  }

  showFileHistory(path: string)
  {
    this.router.navigateByUrl("/" + this.repoPath + "/commits/" + this.hash + "/" + path)
  }
}
