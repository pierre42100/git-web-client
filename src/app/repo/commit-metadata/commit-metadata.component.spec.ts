import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommitMetadataComponent } from './commit-metadata.component';

describe('CommitMetadataComponent', () => {
  let component: CommitMetadataComponent;
  let fixture: ComponentFixture<CommitMetadataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommitMetadataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommitMetadataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
