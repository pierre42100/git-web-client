import { Component, OnInit, Input } from '@angular/core';
import { Commit } from 'src/api/CommitsAPI';

@Component({
  selector: 'app-commit-metadata',
  templateUrl: './commit-metadata.component.html',
  styleUrls: ['./commit-metadata.component.less']
})
export class CommitMetadataComponent implements OnInit {

  @Input("commit") commit: Commit;
  @Input("repo") repo: string;

  get commitURL() : string
  {
    if (!this.commit || !this.commit.hash)
      return "#";

    return "/" + this.repo + "/commit/" + this.commit.hash
  }

  constructor() { }

  ngOnInit(): void {
  }

}
