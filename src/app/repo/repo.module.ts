import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TreeComponent } from './tree/tree.component';
import { RepoNavigationComponent } from './repo-navigation/repo-navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FilesizePipe } from './filesize.pipe';
import { TagsComponent } from './tags/tags.component';
import { BranchesComponent } from './branches/branches.component';
import { RevListItemComponent } from './rev-list-item/rev-list-item.component';
import { TreeFileComponent } from './tree-file/tree-file.component';
import { MarkdownModule } from 'ngx-markdown';
import { HighlightService } from './highlight.service';
import { TimefromnowPipe } from './timefromnow.pipe';
import { CommitsListComponent } from './commits-list/commits-list.component';
import { CommitMetadataComponent } from './commit-metadata/commit-metadata.component';
import { CommitFullInfoComponent } from './commit-full-info/commit-full-info.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { CommitMessageBeginPipe } from './commit-message-begin.pipe';

const routes : Routes =
[
  /**
   * Root page => tree of default page
   */
  {
    component: TreeComponent,
    matcher: (m) => {
      if (m.length == 0)
        return {
          consumed: []
        };
      
      if (m[0].path == "tree")
      {
        return {
          consumed: m
        }
      }
      
      return null;
    }
  },

  /**
   * Commits list
   */
  {
    path: "commits/:rev",
    component: CommitsListComponent,
    children: [
      {
        path: "**",
        component: CommitsListComponent
      }
    ]
  },

  /**
   * Tags page
   */
  {
    path: "tags",
    component: TagsComponent
  },

  /**
   * Branches page
   */
  {
    path: "branches",
    component: BranchesComponent
  },

  /**
   * Commit information
   */
  {
    path: "commit/:hash",
    component: CommitFullInfoComponent
  },
];

@NgModule({
  declarations: [TreeComponent, RepoNavigationComponent, FilesizePipe, TagsComponent, BranchesComponent, RevListItemComponent, TreeFileComponent, TimefromnowPipe, CommitsListComponent, CommitMetadataComponent, CommitFullInfoComponent, CommitMessageBeginPipe],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatDialogModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatMenuModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    OverlayModule,
    MarkdownModule.forChild(),
  ],

  exports: [
    RepoNavigationComponent
  ],

  bootstrap: [RepoNavigationComponent],

  providers: [
    HighlightService
  ]
})
export class RepoModule { }
