import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RevListItemComponent } from './rev-list-item.component';

describe('RevListItemComponent', () => {
  let component: RevListItemComponent;
  let fixture: ComponentFixture<RevListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RevListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RevListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
