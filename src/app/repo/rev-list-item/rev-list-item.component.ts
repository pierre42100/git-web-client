import { Component, OnInit, Input } from '@angular/core';
import { determineDownloadURL } from 'src/app/UrlUtils';

@Component({
  selector: 'app-rev-list-item',
  templateUrl: './rev-list-item.component.html',
  styleUrls: ['./rev-list-item.component.less']
})
export class RevListItemComponent implements OnInit {

  @Input("repoPath") repoPath : string;
  @Input("name") name : string;

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Download a revision as a ZIP archive
   */
  download(ev, rev: string, type: string)
  {
    ev.stopImmediatePropagation()
    location.href = determineDownloadURL(this.repoPath, rev, type);
  }
}
