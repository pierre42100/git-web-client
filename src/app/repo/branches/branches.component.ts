import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BranchesAPI } from 'src/api/BranchesAPI';
import { determineCurrentRepoPath } from 'src/app/UrlUtils';

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.less']
})
export class BranchesComponent implements OnInit {

  repoPath : string
  branches: string[] = []

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe( () => {
      this.repoPath = determineCurrentRepoPath(this.router);
    });

    BranchesAPI.GetList(this.repoPath).then(t => this.branches = t)
  }

}
