import { Component } from '@angular/core';
import { ReposList } from 'src/api/ReposList';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-repos-list',
  templateUrl: './repos-list.component.html',
  styleUrls: ['./repos-list.component.less']
})
export class ReposListComponent {

  prefix = "";
  
  filterInput = new FormControl()

  public get reposList() : String[] {
    let list = ReposList.GetList()
    const filter = this.filterInput.value;

    if (this.prefix != "" && this.prefix)
      list = list.filter(el => el.startsWith(this.prefix))

    if(filter != null)
      list = list.filter(el => el.toLowerCase().includes(filter.toLowerCase()))

    return list
  }

  constructor(private route: ActivatedRoute) {}

  ngOnInit() : void {
    this.route.params.subscribe(p => {
      this.prefix = p.dir;
    });
  }
}
