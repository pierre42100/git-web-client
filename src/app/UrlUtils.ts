/**
 * URL utilities
 * 
 * @author Pierre Hubert
 */

import { BranchesAPI } from "src/api/BranchesAPI";
import { ActivatedRoute, Router, UrlSegment } from '@angular/router';
import { ReposList } from 'src/api/ReposList';
import { apiServerURL } from 'src/utils/ConfUtils';

export function joinURLSegments(u: UrlSegment[]) : string
{
    return u.map(s => s.path).join("/");
}

export function determineCurrentRepoPath(router: Router) : string
{
    const uri = router.url.substr(1);

    for (const repo of ReposList.GetList())
    {
        if (uri == repo || uri.startsWith(repo + "/"))
            return repo;
    }

    throw new Error("Repo for url " + router.url + " not found !")
}

export async function determineCurrentRevision(route: ActivatedRoute, router : Router) : Promise<string>
{
    const repo = determineCurrentRepoPath(router);
    const url = router.url.replace("/" +  repo + "/", "").split("?")[0].split("#")[0];

    if (url.startsWith("commits/") || url.startsWith("tree/"))
    {
        let possibleRevision = url.split("/")[1];
        
        if (possibleRevision.length > 0)
            return possibleRevision;
    }
    return await BranchesAPI.GetDefaultBranch(repo);

}

/**
 * Determine the current tree path
 * 
 * @param router The router
 */
export function determineCurrentTreePath(router: Router) : string
{
    const path = router.url
        .split("/tree/").slice(1).join("/tree/") // Remove first URL part
        .split("/").slice(1).join("/") // Remove revision
        .split("?")[0].split("#")[0] // Query parameters
        ;
    
    return "/" + path;
}

/**
 * Determine the URL where a repository can be cloned
 * 
 * @param repoPath Path to repository
 */
export function determineCloneURL(repoPath: string) : string
{
    return apiServerURL() + repoPath
}

/**
 * Determine the URL where a repository can be downloaded
 * 
 * @param repoPath Path to repository
 * @param rev Target revision
 * @param type The type of the archive
 */
export function determineDownloadURL(repoPath: string, rev: string, type: string) : string
{
    return apiServerURL() + repoPath + "/archive/" + rev + "." + type
}

/**
 * Generate RAW file URL
 * 
 * @param repoPath Repository path
 * @param rev Target revision
 * @param path Path to file
 * @param download Specify whether the file should be downloaded or not
 */
export function determineRawURL(repoPath: string, rev: string, path: string, download: boolean) : string
{
    return apiServerURL() + repoPath + "/raw/" + rev + "/" + path + (download ? "?download" : "");
}

/**
 * Determine the URL where file's history is available
 * 
 * @param repoPath Repository path
 * @param rev Target revision
 * @param path Path to file
 */
export function determineHistoryURL(repoPath: string, rev: string, path: string) : string
{
    return "/" + repoPath + "/commits/" + rev + "/" + path;
}