/**
 * Files utilities
 * 
 * @author Pierre Hubert
 */

/**
 * Given a file path, check whether given file
 * seems to be an image or not
 * 
 * @param path The path of the image
 */
export function isImagePath(path: string) : boolean
{
    return path.toLowerCase().endsWith(".png")
        || path.toLowerCase().endsWith(".jpg")
        || path.toLowerCase().endsWith(".jpeg")
        || path.toLowerCase().endsWith(".gif")
        || path.toLowerCase().endsWith(".bmp")
        || path.toLowerCase().endsWith(".tiff");
}

/**
 * Determine the programming language of a file
 * 
 * @param path The path to the path
 */
export function fileLanguageType(path: string) : string
{
    const filename = path.split("/").slice(-1)[0]

    if (filename == "CMakeLists.txt")
        return "cmake";
    
    if (filename.toLowerCase() == "makefile")
        return "makefile";
    
    const ext = filename.toLowerCase().split(".").slice(-1)[0];
    
    switch(ext)
    {
      case "css":
      return "css";
      
      case "go":
      return "go";
      
      case "ps1":
      return "powershell";
      
      case "json":
      return "json";
      
      case "java":
      return "java";
      
      case "kt":
      return "kotlin";
      
      case "cpp":
      case "c++":
      case "h":
      return "cpp";
      
      case "rs":
      return "rust";
      
      case "rb":
      return "ruby";
      
      case "dart":
      return "dart";
      
      case "swift":
      return "swift";
      
      case "xaml":
      case "xml":
      case "appxmanifest":
      return "xml";
      
      case "yaml":
      case "yml":
      return "yaml";
      
      case "py":
      return "python";
      
      case "js":
      return "javascript";
      
      case "jl":
      return "julia";
      
      case "ts":
      return "typescript";
      
      case "php":
      return "php";
      
      case "cs":
      return "csharp";
      
      case "sh":
      case "bash":
      return "bash";
      
      case "toml":
      return "toml";
      
      case "pl":
      return "perl";
      
      default:
      return null;
    }
}