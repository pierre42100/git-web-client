/**
 * Configuration utilities
 * 
 * @author Pierre Hubert
 */

import { environment } from 'src/environments/environment';

/**
 * Get current API server URL
 * 
 * This method safely wrap the environment variable
 */
export function apiServerURL() : string
{
    if (environment.hasOwnProperty("api_server"))
        return environment["api_server"];

    return location.href.substr(0, location.href.indexOf("/", 9) + 1)
}