/**
 * Files API
 * 
 * @author Pierre Hubert
 */

import { apiServerURL } from 'src/utils/ConfUtils';

export class FilesAPI
{
    /**
     * Check out whether a file is a binary file or
     * something else
     * 
     * @param repo Target repository
     * @param revision Target revision
     * @param path Relative path to file
     */
    public static async IsBinary(repo: string, revision: string, path: string) : Promise<boolean>
    {
        return await (await fetch(apiServerURL() + repo + "/api/files/is_binary/" + revision + "/" + path)).json()
    }
}