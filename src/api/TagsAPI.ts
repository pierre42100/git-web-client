/**
 * Tags API interface
 * 
 * @author Pierre Hubert
 */

import { apiServerURL } from 'src/utils/ConfUtils';

const cache = new Map<string, string[]>();

export class TagsAPI {

    /**
     * Get the list of repositories of a user
     * 
     * @param repo Path to the target repository
     */
    public static async GetList(repo: string) : Promise<string[]>
    {
        if (!cache.has(repo))
            await this.LoadList(repo);
        
        return cache.get(repo);
    }

    /**
     * Load the list of repository
     * 
     * @param repo Path to repository
     */
    private static async LoadList(repo: string) {
        const list = await (await fetch(apiServerURL() + repo + "/api/tags")).json();

        cache.set(repo, list);
    }

}