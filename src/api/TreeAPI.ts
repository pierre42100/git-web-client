/**
 * Tree communication API
 * 
 * @author Pierre Hubert
 */

import { apiServerURL } from 'src/utils/ConfUtils';
import { Commit, CommitsAPI } from './CommitsAPI';

export enum FileType
{
    File,
    Directory
}

export class TreeFile
{
    constructor(
        public type: FileType, 
        public path : string,
        public size: number,
        public object: string
    ) {}

    get isDir() : boolean
    {
        return this.type == FileType.Directory
    }

    get isFile() : boolean
    {
        return this.type == FileType.File
    }


    get name() : string
    {
        return this.path.split("/").slice(-1)[0]
    }
}

export class TreeFileWithCommit
{
    constructor(public readonly file: TreeFile, public readonly commit: Commit) {}
}

export class TreeAPI
{
    /**
     * Fetch the list of files in a repository
     * 
     * @param repo Target repository
     * @param rev Target revision
     * @param path Path in the repository to fetch (INCLUDING THE STARTING SLASH)
     */
    public static async GetFiles(repo: string, rev: string, path: string) : Promise<TreeFile[]>
    {
        const response = await (await fetch(apiServerURL() + repo + "/api/tree/" + rev + path)).json();

        return response.map(this.APIToTreeFile);
    }

    /**
     * Fetch the list of files in a repository, alongside their commits
     * 
     * @param repo Target repository
     * @param rev Target revision
     * @param path Path in the repository to fetch (INCLUDING THE STARTING SLASH)
     */
    public static async GetFilesWithCommits(repo: string, rev: string, path: string) : Promise<TreeFileWithCommit[]>
    {
        const response = await (await fetch(apiServerURL() + repo + "/api/tree_with_commits/" + rev + path)).json();

        return response.map(el => new TreeFileWithCommit(this.APIToTreeFile(el.entry), CommitsAPI.APIToCommit(el.commit)));
    }

    private static APIToTreeFile(el: any) : TreeFile
    {
        return new TreeFile(
            el.is_dir ? FileType.Directory : FileType.File,
            el.file_name,
            el.size,
            el.object
        );
    }
}