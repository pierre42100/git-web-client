/**
 * Branches API
 * 
 * @author Pierre Hubert
 */

import { apiServerURL } from 'src/utils/ConfUtils';

const cache = new Map<string, string[]>();

const defaultBranchesCache = new Map<string, string>();

export class BranchesAPI {

    /**
     * Get the list of branches of a repo
     * 
     * @param repo Path to the target repository
     */
    public static async GetList(repo: string) : Promise<string[]>
    {
        if (!cache.has(repo))
            await this.LoadList(repo);
        
        return cache.get(repo);
    }

    /**
     * Load the list of branches
     * 
     * @param repo Path to repository
     */
    private static async LoadList(repo: string) {
        const list = await (await fetch(apiServerURL() + repo + "/api/branches")).json();

        cache.set(repo, list);
    }

    /**
     * Get the default branch of a repository
     * 
     * @param repo Path to repository
     */
    public static async GetDefaultBranch(repo: string)
    {
        if (!defaultBranchesCache.has(repo))
            defaultBranchesCache.set(repo, await (await fetch(apiServerURL() + repo + "/api/default_branch")).json());

        return defaultBranchesCache.get(repo);
    }
}