/**
 * Statistics API interface
 * 
 * @author Pierre Hubert
 */

import { apiServerURL } from 'src/utils/ConfUtils';

export interface RevStats {
    number_commits: number,
    size: number
}

const cache = new Map<String, RevStats>();

export class StatisticsAPI
{
    /**
     * Repo statistics
     * 
     * @param repo Target repository
     * @param revision Target revision
     */
    public static async GetStats(repo: string, revision: string)
    {
        if (!cache.has(repo + "#" + revision))
            await this.LoadStats(repo, revision);
        
        return cache.get(repo + "#" + revision);
    }

    /**
     * Load statistics
     * 
     * @param repo Target repository
     * @param revision Target revision
     */
    private static async LoadStats(repo: string, revision: string)
    {
        const response = await (await fetch(apiServerURL() + repo + "/api/stats/" + revision)).json();

        cache.set(repo + "#" + revision, {
            number_commits: response.number_commits,
            size: response.size
        });
    }

}