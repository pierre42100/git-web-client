/**
* Commits API
* 
* @author Pierre Hubert
*/

import { fileLanguageType } from 'src/utils/FileUtils';
import { apiServerURL } from 'src/utils/ConfUtils';

export class Commit
{
    hash: string;
    date: Date;
    author_name: string;
    author_mail: string;
    comment: string;
    
    public get firstLineComment() : string
    {
        return this.comment.split("\n")[0]
    }

    public get commentLinesCount() : number
    {
        return this.comment.split("\n").length
    }

    public get authorContact() : string
    {
        return this.author_name + " <" + this.author_mail + ">";
    }
}

export class DiffSection
{
    oldStartLine: number;
    oldLinesCount: number;
    newStartLine: number;
    newLinesCount: number;
    diff: string

    get smallestStartLineNumber() : number
    {
        return this.oldStartLine < this.newStartLine ? this.oldStartLine : this.newStartLine;
    }
}

export interface DiffIndexHeader
{
    hash1: string;
    hash2: string;
    mode ?: number;
}

export interface ExtendedDiffHeader
{
    "OldMode": Number,
    "NewMode": Number,
    "DeletedFileMode": Number,
    "NewFileMode": Number,
    "CopyFrom": String,
    "CopyTo": String,
    "RenameFrom": String,
    "RenameTo": String,
    "SimilarityIndex": Number,
    "DissimilarityIndex": Number,
    "Index": DiffIndexHeader,
}

function GenMap<K extends keyof ExtendedDiffHeader>() : Map<K, ExtendedDiffHeader[K]>
{
    return new Map()
}

export class Diff
{
    source: string;
    dest: string;
    isBinary: boolean;
    extendedHeaders = GenMap();
    sections: DiffSection[]

    get isDeleted(): boolean
    {
        return this.extendedHeaders.has("DeletedFileMode")
    }

    get isCreated(): boolean
    {
        return this.extendedHeaders.has("NewFileMode")
    }

    /**
     * Get the programming language used for this file.
     * 
     * @returns Programming language if found or txt else
     */
    get fileType(): string
    {
        const type = fileLanguageType(this.dest);

        return this.isBinary || type == null ? "txt" : type;

    }
}

export class FullCommit
{
    info: Commit;
    diffs: Diff[];
}

export class CommitsAPI
{
    /**
    * Get the list of commits for a specific revision of a specific
    * repository
    * 
    * @param repo Target repository
    * @param rev Target revision
    * @param startFrom Number of the first commit to return
    */
    public static async GetList(repo: string, rev: string, startFrom: number): Promise<Commit[]>
    {
        const response = await (await fetch(apiServerURL() + repo + "/api/commits/list/" + rev + "/" + startFrom)).json();
        
        return response.map(CommitsAPI.APIToCommit)
    }
    
    /**
    * Get the full commit of a repository
    * 
    * @param repo Target repository
    * @param hash Target commit hash
    */
    public static async GetFullCommit(repo: string, hash: string) : Promise<FullCommit>
    {
        const response = await (await fetch(apiServerURL() + repo + "/api/commits/full/" + hash)).json();
        
        const commit = new FullCommit();
        commit.info = this.APIToCommit(response.metadata);
        commit.diffs = [];
        
        for (const diffJson of response.diffs)
        {
            const diff = new Diff();
            commit.diffs.push(diff);
            diff.source = diffJson.source;
            diff.dest = diffJson.dest;
            diff.isBinary = diffJson.is_binary;
            diff.extendedHeaders = new Map();
            diff.sections = [];
            
            // Process extended headers
            for (const headerJson of diffJson.extended_headers)
            {
                for (const key in headerJson) {
                    if (headerJson.hasOwnProperty(key)) {
                        const element = headerJson[key];
                        diff.extendedHeaders.set(<keyof ExtendedDiffHeader>key, element);
                    }
                }
            }

            // Process sections
            for (const sectionJson of diffJson.diff_sections)
            {
                const section = new DiffSection();
                section.oldStartLine = sectionJson.old_start_line;
                section.oldLinesCount = sectionJson.old_lines_count;
                section.newStartLine = sectionJson.new_start_line;
                section.newLinesCount = sectionJson.new_lines_count;
                section.diff = sectionJson.diff;
                diff.sections.push(section);
            }
        }
        
        return commit;
    }

    /**
     * Get the commits history of a given path
     * 
     * @param repo Target repository
     * @param rev Related revision
     * @param path Path to file
     */
    public static async GetPathHistory(repo: string, rev: string, path: string) : Promise<Commit[]>
    {
        const response = await (await fetch(apiServerURL() + repo + "/api/history/" + rev + "/" + path)).json();

        return response.map(CommitsAPI.APIToCommit)
    }

    /**
     * Get the parent commit of a given commit
     * 
     * @param repo Target repository
     * @param hash Search hash
     * @throws In case of error, or if no parent could be found
     */
    public static async GetParent(repo: string, hash: string) : Promise<Commit>
    {
        const response = await (await fetch(apiServerURL() + repo + "/api/commits/parent/" + hash)).json();

        return CommitsAPI.APIToCommit(response)
    }
    
    /**
    * Turn a commit API entry into a commit object
    */
    public static APIToCommit(obj: any) : Commit
    {
        const c = new Commit();
        c.hash = obj.hash;
        c.date = new Date(obj.date * 1000);
        c.author_name = obj.author_name;
        c.author_mail = obj.author_mail;
        c.comment = obj.comment;
        return c;
    }
}