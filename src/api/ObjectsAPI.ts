/**
 * Objects api
 * 
 * @author Pierre Hubert
 */

import { Commit, CommitsAPI } from './CommitsAPI';
import { apiServerURL } from 'src/utils/ConfUtils';

export class ObjectsAPI
{
    /**
     * Map a list of objects to there corresponding commits
     */
    public static async MapObjectsIDToCommits(repo: string, objs: string[]) : Promise<Map<string, Commit>>
    {
        const response = await (await fetch(apiServerURL() + repo + "/api/objects/commits",
        {
            method: "POST",
            body: JSON.stringify(objs)
        })).json();

        // Parse the list of commits
        const commits: Commit[] = response.commits.map(CommitsAPI.APIToCommit);
        
        const res = new Map<string, Commit>()
        for (const objId of objs) {
            const elHash = response.mapping[objId];
            res.set(objId, commits.find(c => c.hash == elHash))
        }

        return res;
    }
}