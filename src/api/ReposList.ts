/**
 * Fetch & store the list of repositories
 * 
 * @author Pierre Hubert
 */

import { apiServerURL } from 'src/utils/ConfUtils';

let _reposCache: string[] = null;

export class ReposList {

    /**
     * Load the list of repositories
     */
    public static async Load() : Promise<void>
    {
        _reposCache = await (await fetch(apiServerURL() + "api/repos_list")).json();
    }

    /**
     * Get the list of repositories
     */
    public static GetList() : string[]
    {
        return _reposCache;
    }
}

/**
 * Load list of repositories at initial startup
 */
export function loadReposList()
{
    return async () => {
        try
        {
            await ReposList.Load();
        }
        
        catch(e)
        {
            console.error(e);
            alert("Failed to initialize the list of repositories!");
        }
    }
}